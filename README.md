# frontend-challenge

![preview](./frontend-challenge-preview.png)

> *Preview do desafio, no link abaixo tem as especificações visuais de cada componente*

O cheiro e nostalgia da infância estava em todos os lugares, 1 de janeiro de 2038, uma simples caminhada ao acordar parecia como se estivesse dentro de um sonho
foi o dia em que tudo mudou. Os pokemons agora eram reais e as pessoas tinham que se adaptar. A Voltbras logo aproveitou a oportunidade e resolveu usar os pokemons
do tipo elétrico para produzir e armazenar energia para carregar os veículos elétricos, o problema é que ela nāo sabe quais sāo os pokemons do tipo elétrico.

Você é um(a) programador(a) e teve a brilhante idéia de revolucionar o mercado de carregamento através dos pokemons disponibilizando de forma fácil todos os 
pokemons e facilmente encontrar os elétricos. Pesquisando na internet você descobriu que havia uma API com todas as informaçōes necessárias pra iniciar
sua jornada. Entretanto alguém sem conhecimentos em GraphQL nāo saberia como usá-la, entāo é aqui que sua missāo começa.

O seu trabalho é ajudar a Voltbras a reconhecer esses pokemóns para começarem sua jornada de trabalho como abastecedores de carros elétricos.

Para isso:
- utilize a [API pública de GraphQL sobre pokemons](https://graphql-pokemon2.vercel.app/), o que te possibilita obter todas as informaçōes necessárias sobre os pokemons
- siga fielmente a UI proposta, todas as dimensōes, ícones e layout podem ser acessados [AQUI](https://xd.adobe.com/view/4b089b6a-2994-48d8-42f7-3ce2d730c233-3d39/) (Apenas o conteúdo da página **Lista** deve ser implementado)
- disponibilize essas informaçōes em uma página web de forma que o usuário mesmo sem conhecimentos de GraphQL ou de programaçåo consiga aprender tudo que precisa


## Requisitos
Sinta-se livre para fazer qualquer um dos proximos requisitos diferente do que foi pedido desde que consiga justificar a mudança.
Ex.: não fiz o requisito de tal maneira pois a implementação que eu fiz é mais perfomatica e segura.
- [ ] Crie uma landing page utilizando o framework React consumindo os dados da API exibindo a lista de pokemons conforme [layout proposto]( https://xd.adobe.com/view/4b089b6a-2994-48d8-42f7-3ce2d730c233-3d39/);
- [ ] Implemente uma opçāo de filtrar os resultados da lista de pokemons
      **(Deve ser possível adicionar quantos filtros o usuário quiser)**;
    - [ ] Filtrar pelo tipo do pokémon (types);
        - ex: `Grass` => retorna todos os pokemons que sāo do tipo `Grass`;
        - ex: `Grass` + `Water` => retorna todos os pokemons que sāo do tipo `Grass` OU do tipo `Water` ;
    - [ ] Filtrar pelo máximo de CP (maxCP) - ex: "800" => retorna todos os pokemons que possuem 800 de maxCP ou mais que isso.;
    - [ ] Filtrar pelo tipo e pelo máximo de CP - ex: "Type: `Water` + `Grass`; 100 < maxCP < 500" => retorna todos os pokemons que sāo do tipo `Grass` OU do tipo `Water` E cujo maxCP está entre 100 e 500;
- [ ] Deixe aberto em algum repositório open-source (gitlab, github, etc...);
- [ ] Documente o seu projeto, e explique como rodar ele;
- [ ] Adaptar a página para dispositivos móveis (torná-la responsiva);
- [ ] Usar Typescript;
- [ ] Envie junto com o repositório do seu desafio um vídeo curto demonstrando o funcionamento básico do sistema, de formato livre, mostrando as funcionalidades implementadas;
# Extras
- [ ] Adicione testes usando [Jest] ou qualquer outro framework para testes;
- [ ] Hospedar a landing page em algum serviço sem que seja preciso rodar local;
- [ ] Implementar lazy loading;

OBS:Será levado em consideração a modelagem e estrutura de componentes escolhida pelo desenvolvedor, pensando sempre na reutilização dos componentes, fácil manutenção e entendimento.

## Cores do CP

- 0-500 - **#F87060**
- 501-1000 - **#662C91**
- 1001-1500 - **#F5B700**
- 1501-Max - **#00C1FD**

## Exemplo do dado da API de Pokemon
```json
{
  "data": {
    "pokemons": [
      {
        "name": "Bulbasaur",
        "attacks": {
          "special": [
            {
              "name": "Power Whip"
            },
            {
              "name": "Seed Bomb"
            },
            {
              "name": "Sludge Bomb"
            }
          ]
        },
        "number": "001",
        "image": "https://img.pokemondb.net/artwork/bulbasaur.jpg",
        "types": [
          "Grass",
          "Poison"
        ],
        "maxCP": 951
      },
      {
        "name": "Ivysaur",
        "attacks": {
          "special": [
            {
              "name": "Power Whip"
            },
            {
              "name": "Sludge Bomb"
            },
            {
              "name": "Solar Beam"
            }
          ]
        },
        "number": "002",
        "image": "https://img.pokemondb.net/artwork/ivysaur.jpg",
        "types": [
          "Grass",
          "Poison"
        ],
        "maxCP": 1483
      }
    ]
```
